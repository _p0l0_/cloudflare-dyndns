module gitlab.com/_p0l0_/cloudflare-dyndns/src

go 1.12

require (
	github.com/cloudflare/cloudflare-go v0.11.2
	github.com/glendc/go-external-ip v0.0.0-20170425150139-139229dcdddd
	github.com/hashicorp/go-version v1.2.0 // indirect
	github.com/mitchellh/gox v1.0.1 // indirect
	github.com/stretchr/testify v1.4.0
	golang.org/x/crypto v0.0.0-20200221231518-2aa609cf4a9d // indirect
	golang.org/x/lint v0.0.0-20200130185559-910be7a94367 // indirect
	golang.org/x/mod v0.2.0 // indirect
	golang.org/x/net v0.0.0-20200222125558-5a598a2470a0 // indirect
	golang.org/x/sync v0.0.0-20190911185100-cd5d95a43a6e // indirect
	golang.org/x/sys v0.0.0-20200219091948-cb0a6d8edb6c // indirect
	golang.org/x/text v0.3.2 // indirect
	golang.org/x/tools v0.0.0-20200221224223-e1da425f72fd // indirect
	golang.org/x/xerrors v0.0.0-20191204190536-9bdfabe68543 // indirect
)
